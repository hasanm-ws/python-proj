------------------------------------------------------------------------
-------------------- Start: Fetch Configuration ------------------------
------------------------------------------------------------------------
DEFAULT_TIMEOUT_PROCESS_MINUTES=15
projectPerFolder=false
connectionTimeOut=60
checkPolicies=false
serviceUrl=https://dev.whitesourcesoftware.com/agent
forceCheckAllDependencies=false
forceUpdate=false
forceUpdateFailBuildOnPolicyViolation=false
updateTypeValue=OVERRIDE
updateInventory=true
generateScanReport=false
scanReportTimeoutMinutes=10
analyzeFrameworks=false
analyzeFrameworkReference=analyzeFrameworks.txt
offline=false
zip=false
prettyJson=true
resolveAllDependencies=true
ignoreSourceFiles=false
npm: {npmRunPreStep=false, appPath=null
npmIgnoreScripts=false, npmResolveDependencies=true, npmIncludeDevDependencies=false, npmTimeoutDependenciesCollector=60, npmIgnoreNpmLsErrors=false, npmYarnProject=false, npmYarnFrozenLockfile=false, npmIgnoreSourceFiles=true, npmIdentifyByNameAndVersion=false, npmProjectNameFromDependencyFile=false, }
bower: {bowerResolveDependencies=true, bowerRunPreStep=false, bowerIgnoreSourceFiles=false, }
nuget: {nugetResolveDependencies=true, nugetRestoreDependencies=false, nugetRunPreStep=false, nugetIgnoreSourceFiles=true, nugetResolvePackagesConfigFiles=true, nugetResolveCsProjFiles=true, nugetResolveNuspecFiles=true, nugetPreferredEnvironment=, nugetPackagesDirectory=, antResolveDependencies=true
}
maven: {mavenResolveDependencies=true, mavenIgnoredScopes=, mavenAggregateModules=false, mavenIgnorePomModules=true, mavenIgnoreSourceFiles=false, mavenRunPreStep=false, mavenIgnoreDependencyTreeErrors=false, mavenEnvironmentPath=, mavenM2Path=, }
python: {pythonResolveDependencies=true, pipPath=pip, pythonPath=python, pythonIgnorePipInstallErrors=true, pythonInstallVirtualenv=false, pythonResolveHierarchyTree=true, pythonRequirementsFileIncludes=[requirements.txt, Pipfile], pythonResolveSetupPyFiles=false, pythonIgnoreSourceFiles=true, ignorePipEnvInstallErrors=false, pipenvInstallDevDependencies=false, pythonIndexUrl=, runPipenvPreStep=false, pythonIsWssPluginInstalled=false, pythonUninstallWssPlugin=false, }
gradle: {gradleResolveDependencies=true, gradleRunAssembleCommand=true, gradleAggregateModules=false, gradlePreferredEnvironment=gradle, gradleLocalRepositoryPath=, gradleBuildFileIncludes=[], gradleIgnoreSourceFiles=false, gradleRunPreStep=false, gradleIgnoredScopes=[], }
paket: {paketResolveDependencies=true, paketIgnoredScopes=, paketRunPreStep=false, paketPath=, paketIgnoreSourceFiles=true, }
go: {goResolveDependencies=true, goDependencyManager=, goCollectDependenciesAtRuntime=false, goGlideIgnoreTestPackages=true, goGlideIgnoreSourceFiles=false, goGradleEnableTaskAlias=false, }
ruby: {rubyResolveDependencies=true, rubyRunBundleInstall=false, rubyOverwriteGemFile=false, rubyInstallMissingGems=false, rubyIgnoreSourceFiles=true, }
php: {phpResolveDependencies=true, phpRunPreStep=false, phpIncludeDevDependencies=false, }
sbt: {sbtResolveDependencies=true, sbtAggregateModules=false, sbtRunPreStep=false, sbtTargetFolder=[], sbtIgnoreSourceFiles=false, }
html: {htmlResolveDependencies=true, }
cocoapods: {cocoapodsResolveDependencies=true, cocoapodsRunPreStep=false, cocoapodsIgnoreSourceFiles=true, }
hex: {hexResolveDependencies=true, hexRunPreStep=false, hexAggregateModules=false, hexIgnoreSourceFiles=true, }
imageNames=
imageTags=
imageDigests=
forceDelete=false
remoteDockerEnabled=false
maxScanImages=0
forcePull=false
maxPullImages=10
loginSudo=true
amazonRegistryIds=[]
remoteDockerAmazonEnabled=false
amazonRegion=east
amazonMaxPullImages=0
remoteDockerAzureEnabled=false
azureUserName=
azureUserPassword=
azureRegistryNames=[]
remoteDockerArtifactoryEnabled=false
artifactoryUrl=
artifactoryUserName=
artifactoryUserPassword=
artifactoryRepositoryNames=[]
remoteDockerGoogleContainerEnabled=false
googleRepositoryNames=[]
googleActiveAccount=
serverlessConfiguration=
fileListPath=
dependencyDirs=[Data]
configFilePath=whitesource-fs-agent.config
includes=[**/*.java**/*.c**/*.c, **/*.cc, **/*.cp, **/*.cpp, **/*.cxx, **/*.c++, **/*.h, **/*.gem, **/*.tgz, **/*.dll, **/*.js, **/*.py, **/*.jar, **/*.rb, **/*.js, **/*.php, **/*.rpm, **/*.java**/*nupkg**/*.zip]
excludes=[]
dockerIncludes=[.*.*]
dockerExcludes=[]
dockerContainerIncludes=[.*.*]
dockerContainerExcludes=[]
pythonRequirementsFileIncludes=[requirements.txt]
archiveExtractionDepth=0
archiveIncludes=[]
archiveExcludes=[]
followSymlinks=true
dockerScan=false
globCaseSensitive=false
projectPerFolderIncludes=[*]
projectPerFolderExcludes=[]
excludeDependenciesFromNodes=[]
enableGenerateProjectDetailsJson=false
apiToken=******
userKey=
projectVersion=
projectToken=
projectPerSubFolder=false
requesterEmail=
productToken=
productName=WST_325
productVersion=
projectName=WST_325
scanComment=
requireKnownSha1=true
scanReportFileNameFormat=project_with_timestamp
scanPackageManager=false
scanDockerImages=false
scanDockerContainers=false
scanServerlessFunctions=false
logLevel=info
}
------------------------------------------------------------------------
-------------------- End: Fetch Configuration --------------------------
------------------------------------------------------------------------
[INFO] [2019-04-22 07:25:27,040 +0000] - Starting analysis
[INFO] [2019-04-22 07:25:27,040 +0000] - Scanning directories [/home/ubuntu/GitHubRepos/plugins-automation/fsa/tests/Python/pip/WST_325/Data] for package dependencies (may take a few minutes)
[INFO] [2019-04-22 07:25:27,041 +0000] - Included file types: **/*.java**/*.c**/*.c,**/*.cc,**/*.cp,**/*.cpp,**/*.cxx,**/*.c++,**/*.h,**/*.gem,**/*.tgz,**/*.dll,**/*.js,**/*.py,**/*.jar,**/*.rb,**/*.js,**/*.php,**/*.rpm,**/*.java**/*nupkg**/*.zip
[INFO] [2019-04-22 07:25:27,041 +0000] - Excluded file types: 
[INFO] [2019-04-22 07:25:27,042 +0000] - 
------------------------------------------------------------------------
-------------------- Start: Scan Files Matching 'Includes' Pattern -----
------------------------------------------------------------------------
[INFO] [2019-04-22 07:25:27,054 +0000] - 
------------------------------------------------------------------------
-------------------- End: Scan Files Matching 'Includes' Pattern -------
------------------------------------------------------------------------
[INFO] [2019-04-22 07:25:27,056 +0000] - Attempting to resolve dependencies
[INFO] [2019-04-22 07:25:27,057 +0000] - 
------------------------------------------------------------------------
-------------------- Start: Pre-Step & Resolve Dependencies ------------
------------------------------------------------------------------------
[INFO] [2019-04-22 07:25:27,091 +0000] - Trying to resolve PYTHON dependencies
[INFO] [2019-04-22 07:25:27,093 +0000] - topFolder = /home/ubuntu/GitHubRepos/plugins-automation/fsa/tests/Python/pip/WST_325/Data
[WARN] [2019-04-22 07:25:27,493 +0000] - Fail to run 'pip install -r /home/ubuntu/GitHubRepos/plugins-automation/fsa/tests/Python/pip/WST_325/Data/requirements.txt'. To see the full error, re-run the plugin with this parameter in the config file: log.level=debug
[INFO] [2019-04-22 07:25:27,494 +0000] - Try to download each dependency in /home/ubuntu/GitHubRepos/plugins-automation/fsa/tests/Python/pip/WST_325/Data/requirements.txt file one by one. It might take a few minutes.
[WARN] [2019-04-22 07:25:27,699 +0000] - Failed to download the transitive dependencies of 'docopt==0.6.1'
[WARN] [2019-04-22 07:25:27,701 +0000] - Failed to download the transitive dependencies of '-e git+https://github.com/crsmithdev/arrow'
[INFO] [2019-04-22 07:25:27,706 +0000] - 
------------------------------------------------------------------------
-------------------- End: Pre-Step & Resolve Dependencies --------------
------------------------------------------------------------------------
[INFO] [2019-04-22 07:25:27,707 +0000] - Total dependencies found for all resolvers: 0 (0 unique)
[INFO] [2019-04-22 07:25:27,708 +0000] - Scanning directories [/home/ubuntu/GitHubRepos/plugins-automation/fsa/tests/Python/pip/WST_325/Data] for matching source/binary file types (may take a few minutes)
[INFO] [2019-04-22 07:25:27,712 +0000] - Total files found according to the includes/excludes pattern: 0
[INFO] | [                                 ] 0% - 0 of 0 files
                                                                                  
[INFO] [2019-04-22 07:25:27,714 +0000] - Finished analyzing Files
[INFO] [2019-04-22 07:25:27,718 +0000] - Removing empty project WST_325 from update (found 0 matching files)
[INFO] [2019-04-22 07:25:27,719 +0000] - Exiting, nothing to update
[INFO] [2019-04-22 07:25:27,725 +0000] - 
------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------- WhiteSource Scan Summary: --------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------
======================================================================================================================================================

======================================================================================================================================================
Step                                 Completion Status                              Elapsed                              Comments                              
======================================================================================================================================================
Fetch Configuration                     COMPLETED                                 00:00:00.096                           --------
Scan Files Matching 'Includes' Pattern  COMPLETED                                 00:00:00.012                   0 source/binary files
Pre-Step & Resolve Dependencies         COMPLETED                                 00:00:00.649                   0 dependencies
   PYTHON                               COMPLETED                                 00:00:00.615                   0 dependencies

======================================================================================================================================================
Elapsed running time:                                                             00:00:00.757
======================================================================================================================================================
Process finished with exit code SUCCESS (0)
